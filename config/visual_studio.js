const config = {
    "http.proxyStrictSSL": false,
    "editor.renderWhitespace": "boundary",
    "eslint.enable": true,
    "typescript.validate.enable": false,
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",

    "themes": ['Ayu']
};